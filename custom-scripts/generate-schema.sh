#!/bin/bash

# Arguments handling

# Default values
output_file="lib/schema.ts"

usage() {
    printf "Usage: %s [options...] <openApiUrl> [other options...]\n" "$(basename $0)"
    printf " -h \t\tDisplay this message\n"
    printf " -o <file>\tFile to write the schema to. Defaults to '$output_file'\n"
    printf " [other options...]\t Options that are passed down to generation binary (openapi-typescript)"
    echo 
    echo "Generate typescript schema based on OpenAPI specification."
    echo "It might be useful for easier types update after changing it."
}

# Parsing/handling
options=`getopt ho: $*`
if [ $? -ne 0 ]; then
    usage
    exit 2
fi
set -- $options
while :; do
    case "$1" in
    -h)
        usage
        shift
        exit 0
        ;;
    -o)
        output_file="$2"
        shift; shift
        ;;
    --)
        shift; break
        ;;
    esac
done

# Get positional argument and verify its presence
url=$1
shift;
if [ -z "$url" ]
then
    echo "Please specify URL."
    echo "usage: -h"
    exit 1
fi

# Arguments are ready

echo "Downloading the api specification..."
temp_file_name="/tmp/typescript-gen-schema-script.openapi.json"
curl --output $temp_file_name https://staging-yacsp.markovvn1.me/openapi.json
echo

echo "Generating schema..."
set -o xtrace
npx openapi-typescript $temp_file_name --output $output_file "$@"
