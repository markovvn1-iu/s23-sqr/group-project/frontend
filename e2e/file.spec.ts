import { Page } from '@playwright/test';
import { test, expect } from 'playwright-test-coverage'
import { passCaptcha } from './common';

test.describe("file view", () => {
    test.beforeEach(async ({ page, context }) => {
        await page.goto("http://localhost:3000/files/1");
        await passCaptcha(page);
        await page.reload();
    });

    test('file view works', async ({ page }) => {
        // Check (and wait for) necessary fields to be displayed
        await expect(page).toHaveTitle(/YACSP/);
        await expect(page.getByText("file title")).toHaveCount(1);
        await expect(page.getByText("language name")).toHaveCount(1);
        await expect(page.getByText("mocked file with id")).toHaveCount(1);
        await expect(page).toHaveScreenshot("file-view.png");

        // Test star button
        await expect(page.getByRole('button', { name: 'Star' }).getByText("0")).toHaveCount(1)
        await page.getByRole('button', { name: 'Star' }).click();
        // button click scrolls the page
        await page.evaluate(() => window.scrollTo(0, 0));
        await expect(page.getByRole('button', { name: 'Star' }).getByText("1")).toHaveCount(1)
        await expect(page).toHaveScreenshot("file-view-star-pressed.png");
        await page.getByRole('button', { name: 'Star' }).click();
        // button click scrolls the page
        await page.evaluate(() => window.scrollTo(0, 0));
        await expect(page.getByRole('button', { name: 'Star' }).getByText("0")).toHaveCount(1)
        await expect(page).toHaveScreenshot("file-view.png");

        // Copy buttons test
        await page.getByRole('button', { name: 'Copy url' }).click();
        let clipboardUrl = await page.evaluate("navigator.clipboard.readText()");
        expect(clipboardUrl).toContain("http://localhost:3000/files/1");
        await page.getByRole('button', { name: 'Copy content' }).click();
        let clipboardContent = await page.evaluate("navigator.clipboard.readText()");
        expect(clipboardContent).toContain("mocked file with id 1");
    });
});

