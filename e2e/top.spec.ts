import { Page } from '@playwright/test';
import { test, expect } from 'playwright-test-coverage';
import { passCaptcha } from './common';

test.describe("top", () => {
    test.beforeEach(async ({ page, context }) => {
        await page.goto("http://localhost:3000/top");
        await passCaptcha(page);
    });

    test('has title', async ({ page }) => {
        await expect(page).toHaveTitle(/YACSP/);
    });

    test('displays correctly', async ({ page }) => {
        await expect(page.locator('h2').filter({ hasText: /.+"Hello, World"/ })).toHaveCount(2);
        // file entry
        await expect(page.getByText(/rust/)).toHaveCount(1);
        await expect(page.locator('code').filter({ hasText: /.*println!.+/ })).toHaveCount(1);
        await expect(page.getByText('9999 ★')).toHaveCount(1);
        // file entry
        await expect(page.getByText(/python/)).toHaveCount(1);
        await expect(page.locator('code').filter({ hasText: /.*print\(\".+/ })).toHaveCount(1);
        await expect(page.getByText('-1 ★')).toHaveCount(1);

        await expect(page).toHaveScreenshot("top.png");
    });
});

