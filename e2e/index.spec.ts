import { Page } from '@playwright/test';
import { test, expect } from 'playwright-test-coverage';
import { passCaptcha } from './common';

test.describe("index", () => {
  test.beforeEach(async ({ page, context }) => {
    await page.goto("http://localhost:3000/");
    // Don't add cookie since we can obtain it with mock environment
    // await context.addCookies([{name: "token", value: "jwt", domain: "localhost", path: "/"}]);
  });

  test('has title', async ({ page }) => {
    await expect(page).toHaveTitle(/YACSP/);
  });

  test('create file', async ({ page }) => {
    // wait for text editor to load
    await expect(page.getByText('Paste your code here...')).toHaveCount(1);
    await expect(page).toHaveScreenshot("index-empty.png");
    // fill in the form
    await page.getByRole('combobox').selectOption('3,python');
    await page.getByRole('combobox').selectOption('1,javascript');
    await page.getByRole('textbox').first().fill("const a = () => {}");
    await page.getByPlaceholder('filename.ex').fill("abobus.js");
    await page.getByRole('group').filter({ hasText: 'Make it private' }).locator('span').first().click();
    await page.getByRole('button', {name: "Save and share"}).click();
    await passCaptcha(page);
    // Resend the form since now we have token; it's intended
    await page.getByRole('button', { name: "Save and share" }).click();
    await expect(page).toHaveURL(/files\/(.+)/);
    await expect(page.getByText(/mocked file with id .+/)).toHaveCount(1);
    await page.getByRole('link', { name: 'YACSP' }).click();
    await expect(page).toHaveURL("/");
  })
});
