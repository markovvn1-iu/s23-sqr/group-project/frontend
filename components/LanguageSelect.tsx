import { Language } from "@/pages/api/languages";
import { Flex, Select, Stack } from "@chakra-ui/react";
import { ChangeEventHandler, useEffect, useState } from "react";

interface Props {
    languages: Language[],
    onChange: ChangeEventHandler<HTMLSelectElement>,
    value: any,
}


export default function LanguageSelect({languages, onChange, value}: Props) {
    return (
        <Select variant={"outline"} onChange={onChange} value={value} placeholder='Select language'>
            {languages.map(lang => {
                return <option key={lang.id} value={[String(lang.id), lang.name]}>{lang.name}</option>
            })}
        </Select>
    )
    
}