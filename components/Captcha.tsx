import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    CircularProgress,
} from '@chakra-ui/react'
import React, { Dispatch, SetStateAction, useState } from 'react';

import ReCAPTCHA from "react-google-recaptcha";


interface CaptchaModalProps {
    setCaptchaToken: Dispatch<SetStateAction<string | undefined>>,
    onCaptchaClose: () => void,
    isCaptchaOpen: boolean,
}

export const CaptchaModal = (captchaState: CaptchaModalProps) => {
    let [isProgressShown, setProgressShown] = useState(false);

    const onCaptchaChange = (token: string | null) => {
        if (token === null) {
            // Captcha expired, don't do anything and let the user
            // complete again. Maybe add message later.
            return;
        } else {
            captchaState.setCaptchaToken(token);
            setProgressShown(true)
            setTimeout(captchaState.onCaptchaClose, 1000);
        }
    };

    return (
        <>
            {/* We only close it on captcha completion, so no meaningful callback onClose */}
            <Modal isOpen={captchaState.isCaptchaOpen} onClose={() => { }}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Complete CAPTCHA</ModalHeader>
                    <ModalBody>
                        <ReCAPTCHA
                            hidden={isProgressShown}
                            sitekey={process.env.NEXT_PUBLIC_CAPTCHA_PUBKEY || ""}
                            onChange={onCaptchaChange}
                        />
                        {isProgressShown && <CircularProgress isIndeterminate />}
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    )
}
