import { useIsOverflow } from "@/lib/hooks";
import { Box, Text } from "@chakra-ui/react";
import { Children, cloneElement, isValidElement, ReactElement, useRef } from "react";

interface Props {
    maxH: string,
    children: ReactElement<{ bgColor?: string }>,
    bgColor: string,
    textColor: string,
}


const OverflowBox = ({ maxH, children, bgColor, textColor }: Props) => {
    const ref = useRef<HTMLDivElement | null>(null);
    const isOverflow = useIsOverflow(ref);
    let bgEndActualColor = bgColor;
    let textEndActualColor = textColor;
    if (isOverflow === true) {
        bgEndActualColor = `rgba(0, 0, 0, 0)`;
        textEndActualColor = `transparent`;
    }

    return <Box ref={ref} maxH={maxH} overflowY="hidden" overflowX="hidden" zIndex={1} position="relative" bgGradient={`linear(to-b, ${bgColor}, ${bgEndActualColor})`}>
        <Text
            bgGradient={`linear(to-b, ${textColor}, ${textColor}, transparent, transparent, transparent)`}
            bgClip='text'
        >
            {cloneElement(children, { bgColor: textEndActualColor })}
        </Text>
    </Box>
};

export default OverflowBox;
