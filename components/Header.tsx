import {
    Box,
    Flex,
    Button,
    useColorModeValue,
    useColorMode,
    Text,
    HStack,
    Link
  } from '@chakra-ui/react';
  import { LinkIcon, MoonIcon, SunIcon } from '@chakra-ui/icons';
import NextLink from 'next/link';


const Logo = () => {
    return <NextLink href="/"><HStack><LinkIcon/><Text>YACSP</Text></HStack></NextLink> 
}

const Header = () => {
    const { colorMode, toggleColorMode } = useColorMode();
    const bgColor = useColorModeValue('gray.100', 'gray.900');

    return (
    <>
      <Box bgColor={ bgColor }>
      <Flex h={[10, 16]} alignItems={'center'} justifyContent={'space-between'} px={5}>
          <Logo/>
          <HStack direction={'row'} spacing={5}>
            <Link href="/top">See top</Link>
              <Button id='toggle-color-mode' onClick={toggleColorMode} size="sm">
                {colorMode === 'light' ? <MoonIcon /> : <SunIcon />}
            </Button>
          </HStack>
        </Flex>
      </Box>
    </>
  );
}

export default Header;