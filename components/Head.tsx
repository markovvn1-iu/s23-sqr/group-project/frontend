import Head from "next/head"

const MyHead = () => {
    return <Head>
        <title>YACSP</title>
        <meta name="description" content="Y" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
    </Head>
};

export default MyHead;
