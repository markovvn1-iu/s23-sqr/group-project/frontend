import { Flex, Heading, LinkBox, LinkOverlay, Text, Card, CardHeader, CardBody, CardFooter, VStack, Stack, Spacer, HStack, Box, useColorMode, useColorModeValue, Link, Code } from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import Page from '../components/Page'
import { components } from '@/lib/schema'
import { CustomPageProps } from './_app'
import { ApiResponse } from 'openapi-typescript-fetch'
import React from 'react'
import OverflowBox from '@/components/OverflowBox'

type File = components["schemas"]["FileInResponse"];

export default function Top(args: CustomPageProps) {
  const [topFiles, setTopFiles] = useState<File[]>([])
  const secondary_text_color = useColorModeValue("gray.500", "gray.400")
  const codeBgColor = useColorModeValue("#edf2f7", "rgba(226, 232, 240, 0.16)")
  const codeTextColor = useColorModeValue("gray.800", "gray.200")

  useEffect(() => {
    args.fetcher.path("/api/top").method("get").create()({ offset: 0, limit: 10 })
      .then((response: ApiResponse<File[]>) => {
        if (!response) {
          console.log("503: no response");
        }
        else if (response.ok) {
          setTopFiles(response.data)
        } else {
          console.log("Received not 'ok' response: " + response.statusText)
        }
      })
  }, [args.fetcher])

  const trim_text_lines = (text: string, maxLines: number) => {
    const lines = text.split('\n');
    if (lines.length <= maxLines) {
      return text
    }
    const linesTrimmed = lines.slice(0, maxLines)
    return linesTrimmed.join('\n')
  }

  return (
      <Page>
      <main>
        <VStack spacing={[4, 8]} mt={10} mb={20}>
          <Stack minWidth={[300, 800]} maxWidth={1200}>
            {topFiles.map((file) => {
              return <LinkBox key={file.uid}>
                <Card key={file.title}>
                <CardHeader>
                    <LinkOverlay href={`files/${file.uid}`}>
                      <Heading>{file.title}</Heading>
                    </LinkOverlay>
                </CardHeader>
                  <CardBody>
                    <OverflowBox maxH="100" bgColor={codeBgColor} textColor={codeTextColor}>
                      <Code display="block" whiteSpace="pre" bgClip="text">{trim_text_lines(file.content, 10)}</Code>
                    </OverflowBox>
                </CardBody>
                <CardFooter width='100%'>
                  <Flex minWidth='max-content' width='100%'>
                    <Box textColor={secondary_text_color}>{file.language.name}</Box>
                    <Spacer />
                    <Box textColor={secondary_text_color}>{file.star_count} ★</Box>
                  </Flex>
                </CardFooter>
                </Card>
              </LinkBox>
            })}
          </Stack>
        </VStack>
        </main>
      </Page>
  )
}
