// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { components } from '@/lib/schema';
import type { NextApiRequest, NextApiResponse } from 'next'

type File = components["schemas"]["FileInResponse"];

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<File[]>
) {
    res.status(200).json([
        {
            uid: "12345678", title: "Rust \"Hello, World\"", content: `fn main {\n    println!("Hello world!! let's test overflow let's test overflow let's test overflow let's test overflow let's test overflow let's test overflow ")\n}\na\nb\nc\nd\n// some_comment lol\n\n\n\n\n\n\n\n //kek`,
            language: { id: 17, name: 'rust' }, visibility: "public", mark: "no_star", star_count: 9999
        },
        {
            uid: "31337", title: "Python \"Hello, World\"", content: `print("Hello world")\nprint("kek")`,
            language: { id: 3, name: 'python' }, visibility: "public", mark: "star", star_count: -1
        }
    ])
}
