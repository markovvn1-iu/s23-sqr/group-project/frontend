// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { fileState } from "../\[id\]"

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    const new_mark = req.body.new_mark;
    fileState.mark = new_mark
    res.status(200).json({})
}
