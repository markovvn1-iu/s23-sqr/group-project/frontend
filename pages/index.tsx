import LanguageSelect from '@/components/LanguageSelect';
import { Button, FormControl, FormLabel, HStack, Input, Stack, Switch, Text, useColorMode, VStack } from '@chakra-ui/react'
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { ApiResponse } from 'openapi-typescript-fetch';
import { ChangeEvent, useEffect, useState } from 'react'
import Page from '../components/Page'
import { Language } from './api/languages';
import { CustomPageProps } from './_app';
const TextEditor = dynamic(import('../components/TextEditor'), {
  ssr: false
})

export default function Home(args: CustomPageProps) {
  const router = useRouter()
  const [title, setTitle] = useState("");
  const [isPrivate, setIsPrivate] = useState(false);
  const [content, setContent] = useState("");
  const [languages, setLanguages] = useState<Language[]>([])
  const [languageName, setLanguageName] = useState("javascript")
  const [languageId, setLanguageId] = useState(1)
  const [isLoading, setLoading] = useState(false);

  const onSubmit = async (e: any) => {
    e.preventDefault()

    let fetcher = args.fetcher;
    const call = fetcher.path("/api/file/new").method("post").create();
    call({
      title: title,
      content: content,
      language_id: languageId,
      visibility: isPrivate ? 'private' : 'public'
    }).then((response: ApiResponse<{uid: string}>) => {
      if (response?.data?.uid) {
        const uid = response.data.uid;
        router.push(`/files/${uid}`);
      }
    })
    
  }

  useEffect(() => {
    setLoading(true)
    args.fetcher.path("/api/languages").method("get").create()({})
      .then((response: any) => {
        setLanguages(response.data)
      })
      .finally(() => setLoading(false))
  }, [args.fetcher])

  const languageChanged = (value: ChangeEvent<HTMLSelectElement>) => {
    const [id, name] = value.target.value.split(',')
    setLanguageName(name)
    setLanguageId(Number(id) || 0)
  }

  return (
      <Page>
        <main>
        <VStack spacing={[4, 8]} mt={10} mb={20}>
          <Text size='lg'>Create new file:</Text>
          <Stack minWidth={[300, 800]} maxWidth={1200}>
            <LanguageSelect languages={languages} value={languageId + ',' + languageName} onChange={languageChanged} ></LanguageSelect>
            <TextEditor language={languageName} content={content} setContent={setContent}/>
            <FormControl isRequired>
              <FormLabel>File name</FormLabel>
              <Input placeholder='filename.ex' value={title} onChange={event => setTitle(event.target.value)} />
            </FormControl>
            <HStack justify="space-between">
              <FormControl display='flex' alignItems='center'>
                <FormLabel htmlFor='is-private' mb='0'>
                  Make it private
                </FormLabel>
                <Switch isChecked={isPrivate} onChange={() => setIsPrivate(prev => !prev)} id='is-private' />
              </FormControl>
              <Button isDisabled={isLoading} size="lg" colorScheme='blue' disabled={isLoading} onClick={onSubmit}>Save and share</Button>
            </HStack>
          </Stack>
        </VStack>
        </main>
      </Page>
  )
}
