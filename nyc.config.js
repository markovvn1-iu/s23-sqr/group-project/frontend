module.exports = {
    exclude: [
        // Some temp folders?
        ".next",
        // Configs
        "babel.config.js", "next-env.d.ts", "next.config.js", "nyc.config.js",
        // Test outputs??
        "coverage",
        // Tests themselves
        "e2e",
        // Mock api
        "pages/api",
    ],
    reporter: ["html", "text", "lcov"],
    all: true,
};
