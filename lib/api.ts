import { Fetcher, ApiError } from "openapi-typescript-fetch";
import Cookies from 'js-cookie';
import { paths } from './schema';
import { FetchConfig, Middleware } from "openapi-typescript-fetch/dist/cjs/types";

export type CustomFetcher = ReturnType<typeof Fetcher.for<paths>>;

/**
 * Configured fetcher for requesting backend.
 * 
 * ## Examples
 *  ```ts
    const call = fetcher.path("/api/file/{file_uid}/mark").method("put").create({ new_mark: true });
    const result = await call({ file_uid: "12345", new_mark: "star" })
    const success = result.status === 200;
    ```
 * @param onCaptchaOpen callback that is called when captcha is needed
 * @returns fetcher
 */

const ignore = [
    "/api/user/register",
    "/api/languages"
]

export const buildCustomFetcher = (onCaptchaOpen: () => void) => {
    const tokenInserter: Middleware = async (url, init, next) => {
        if (!ignore.some(path => url.includes(path))) {
            const cookie = Cookies.get("token");
            if (cookie === undefined) {
                console.log("token not found. open captcha")
                onCaptchaOpen();
                // don't make a request
                // let the user retry last action by themselves
                return undefined as any;
            } else {
                init.headers.append("Authorization", "Bearer " + cookie);
            }
        }
        try {
            const response = await next(url, init);
            return response;
        }
        catch (e) {
            if (e instanceof ApiError) {
                if (e.status === 401) {
                    console.log("token expired, open captcha")
                    // token is expired, apparently
                    onCaptchaOpen();
                    return undefined as any;
                }
            }
            throw e;
        }
    }

    const fetcherConfig: FetchConfig = {
        baseUrl: process.env.NEXT_PUBLIC_BACKEND_API_URL,
        use: [tokenInserter]
    };
    let fetcher = Fetcher.for<paths>();
    fetcher.configure(fetcherConfig);
    return fetcher
}
