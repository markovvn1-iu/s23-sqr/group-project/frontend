import { MutableRefObject, useLayoutEffect, useState } from "react";

export const useIsOverflow = <T extends HTMLElement>(ref: MutableRefObject<T | null>, callback?: (hasOverflow: boolean | null) => void) => {
    const [isOverflow, setIsOverflow] = useState<boolean | null>(null);

    useLayoutEffect(() => {
        const { current } = ref;
        const trigger = () => {
            const hasOverflow = current ? current.scrollHeight > current.clientHeight : null;

            setIsOverflow(hasOverflow);

            if (callback) callback(hasOverflow);
        };

        if (current) {
            trigger();
        }
    }, [callback, ref]);

    return isOverflow;
};