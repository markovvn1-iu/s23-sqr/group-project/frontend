# yacsp frontend

## Technology stack

Core technologies what used in the project are

+ [Typescript](https://www.typescriptlang.org/) as premier language
+ [yarn](https://yarnpkg.com/) as package manager
+ [React](https://reactjs.org/) as UI library
+ [Next.js](https://nextjs.org/) as application framework
+ [Chakra](https://chakra-ui.com/) as component library


## Getting Started

First, do

```bash
npm install
```

Run the development server:

```bash
yarn dev
```

## Development

### API schema generation

To generate/update new schema based on api specification (OpenAPI 3.x), use script `generate-schema.sh` like this:

```bash
./generate-schema.sh https://staging-yacsp.markovvn1.me/openapi.json
```
